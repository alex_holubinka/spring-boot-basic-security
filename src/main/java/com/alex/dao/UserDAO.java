package com.alex.dao;

import com.alex.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * Created by Alex on 22.03.2017.
 */
public interface UserDAO extends JpaRepository<User, Integer> {
    @Query("from User u where u.username =:username")
    public User findByUserName(@Param("username") String username);
}
