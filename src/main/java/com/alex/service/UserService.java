package com.alex.service;

import  com.alex.entity.User;

/**
 * Created by Alex on 22.03.2017.
 */
public interface UserService {
    void save(User user);
    User findByName(String username);
}
