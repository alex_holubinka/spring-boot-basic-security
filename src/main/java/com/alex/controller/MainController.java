package com.alex.controller;

import com.alex.entity.User;
import com.alex.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Created by Alex on 22.03.2017.
 */
@Controller
public class MainController {
    @Autowired
    UserService userService;

    @GetMapping("/")
    public String toIndex(){
        return "index";
    }
    @PostMapping("save")
    public String save(@RequestParam("username") String username,
                            @RequestParam("password") String password){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        userService.save(user);
        return "index";
    }

    @GetMapping("toLogin")
    public String toLogin(){
        return "login";
    }
}
